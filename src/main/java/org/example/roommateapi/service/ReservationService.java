package org.example.roommateapi.service;

import lombok.RequiredArgsConstructor;
import org.example.roommateapi.domain.Reservation;
import org.example.roommateapi.model.ReservationItem;
import org.example.roommateapi.model.ReservationRequest;
import org.example.roommateapi.model.ReservationUpdateRequest;
import org.example.roommateapi.repo.ReservationRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReservationService {
    private final ReservationRepository reservationRepository;

    public void setReservation(ReservationRequest request) {
        reservationRepository.save(new Reservation.Builder(request).build());
    }

    public List<ReservationItem> getReservations() {
        List<Reservation> originList = reservationRepository.findAll();
        List<ReservationItem> result = new LinkedList<>();
        for (Reservation origin : originList) result.add(new ReservationItem.Builder(origin).build());
        return result;
    }

    public void putReservation(long id, ReservationUpdateRequest request) {
        Reservation reservation = reservationRepository.findById(id).orElseThrow();
        reservation.putInfo(request.getDateRes(), request.getPhoneNumber());
        reservationRepository.save(reservation);
    }

    public void delReservation(long id) {
        reservationRepository.deleteById(id);
    }
}
