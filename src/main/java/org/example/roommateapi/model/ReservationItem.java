package org.example.roommateapi.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.example.roommateapi.domain.Reservation;
import org.example.roommateapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReservationItem {
    private Long id;
    private String customerInfo; // 홍길동 (010-000-0000)
    private LocalDate dateRes;

    private ReservationItem(Builder builder) {
        this.id = builder.id;
        this.customerInfo = builder.customerInfo;
        this.dateRes = builder.dateRes;
    }

    public static class Builder implements CommonModelBuilder<ReservationItem> {
        private final Long id;
        private final String customerInfo; // 홍길동 (010-000-0000)
        private final LocalDate dateRes;

        public Builder(Reservation reservation) {
            this.id = reservation.getId();
            this.customerInfo = reservation.getCustomerName() + " (" + reservation.getPhoneNumber() + ")";
            this.dateRes = reservation.getDateRes();
        }

        @Override
        public ReservationItem build() {
            return new ReservationItem(this);
        }
    }
}
