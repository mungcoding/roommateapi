package org.example.roommateapi.model;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class ReservationUpdateRequest {
    @NotNull
    private LocalDate dateRes;

    @NotNull
    @Length(min = 13, max = 20)
    private String phoneNumber;
}
