package org.example.roommateapi.model;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.example.roommateapi.enums.RoomInfo;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class ReservationRequest {
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoomInfo roomInfo;

    @NotNull
    private LocalDate dateRes;

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 13, max = 20)
    private String phoneNumber;
}
