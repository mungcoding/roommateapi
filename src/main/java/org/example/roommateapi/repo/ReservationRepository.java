package org.example.roommateapi.repo;

import org.example.roommateapi.domain.Reservation;
import org.example.roommateapi.enums.RoomInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findAllByRoomInfo(RoomInfo roomInfo);
}
