package org.example.roommateapi.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.example.roommateapi.model.ReservationItem;
import org.example.roommateapi.model.ReservationRequest;
import org.example.roommateapi.model.ReservationUpdateRequest;
import org.example.roommateapi.service.ReservationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reservation")
@RequiredArgsConstructor
public class ReservationController {
    private final ReservationService reservationService;

    @PostMapping("/room")
    public String setRoom(@RequestBody @Valid ReservationRequest request) {
        reservationService.setReservation(request);
        return "응";
    }

    @GetMapping("/all")
    public List<ReservationItem> getReservations() {
        return reservationService.getReservations();
    }

    @PatchMapping("/info/{id}")
    public String putInfo(@PathVariable long id, @RequestBody @Valid ReservationUpdateRequest request) {
        reservationService.putReservation(id, request);
        return "어";
    }

    @DeleteMapping("/{id}")
    public String delReservation(@PathVariable long id) {
        reservationService.delReservation(id);
        return "어 지웟어";
    }

}
