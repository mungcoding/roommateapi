package org.example.roommateapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
