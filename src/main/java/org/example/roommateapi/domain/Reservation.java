package org.example.roommateapi.domain;

import jakarta.persistence.*;
import lombok.*;
import org.example.roommateapi.enums.RoomInfo;
import org.example.roommateapi.interfaces.CommonModelBuilder;
import org.example.roommateapi.model.ReservationRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(length = 10, nullable = false)
    private RoomInfo roomInfo;

    @Column(nullable = false)
    private LocalDate dateRes;

    @Column(length = 20, nullable = false)
    private String customerName;

    @Column(length = 20, nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private Reservation(Builder builder) {
        this.roomInfo = builder.roomInfo;
        this.dateRes = builder.dateRes;
        this.customerName = builder.customerName;
        this.phoneNumber = builder.phoneNumber;
        this.dateCreate = builder.dateCreate;
    }

    public void putInfo(LocalDate dateRes, String phoneNumber) {
        this.dateRes = dateRes;
        this.phoneNumber = phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<Reservation> {
        private final RoomInfo roomInfo;
        private final LocalDate dateRes;
        private final String customerName;
        private final String phoneNumber;
        private final LocalDateTime dateCreate;

        public Builder(ReservationRequest request) {
            this.roomInfo = request.getRoomInfo();
            this.dateRes = request.getDateRes();
            this.customerName = request.getCustomerName();
            this.phoneNumber = request.getPhoneNumber();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Reservation build() {
            return new Reservation(this);
        }
    }
}
