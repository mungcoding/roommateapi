package org.example.roommateapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoomInfo {
    R_101("101호", 20),
    R_103("103호", 9),
    R_102("102호", 4);

    private final String name;
    private final int area;
}
